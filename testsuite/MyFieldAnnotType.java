import java.lang.annotation.*;
import static java.lang.annotation.ElementType.*;


@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value={FIELD})
public @interface MyFieldAnnotType {
    
    public enum myEnum {
    ONE, TWO, THREE
}
    
    myEnum evalue();
    Class clvalue();
}
