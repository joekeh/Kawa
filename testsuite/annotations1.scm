(define-alias ElementType java.lang.annotation.ElementType)
;(define-constant XX 20)

(define-class MyClass ()
  (@MyAnnotType name: "MyClassName")
  interface: #f
  (x #|(@java.lang.Deprecated)|#
   ;;     (@javax.jws.soap.InitParam name: (make-string 4)  value: 0)
   ;; Also array of annotation: SOAPMessageHandler
   ;(@javax.xml.bind.annotation.XmlSchemaTypes({ @javax.xml.bind.annotation.XmlSchemaType(...), @XmlSchemaType(...) })
   (|@MyAnnotType| ;; Test deprecated single-symbol form
    svalue: 4324
    name: "myName"
    ;names: #("name1" "name2")
    bvalue: 2
    ivalue: (+ 100 12)
    blvalue: (> 3 4)
    chvalue: #\B
    etype: ElementType:PACKAGE
    )
   ::integer)
  (y
   (@MyFieldAnnotType 
    evalue: MyFieldAnnotType:myEnum:TWO
    clvalue: java.util.ArrayList)
   :: integer)
  ((toString)
   (@java.lang.Override)
   (@MyAnnotType
    names: (string[] "x" "y")
    clvalue: java.io.InputStream)
   (format "MyClass[x:~s y:~s]" x y))
)
#| 
     (@java.beans.ConstructorProperties value: #("abc" "def"))
     ::integer))
|#
